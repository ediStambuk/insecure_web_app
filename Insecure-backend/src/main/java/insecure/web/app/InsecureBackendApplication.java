package insecure.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsecureBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsecureBackendApplication.class, args);
    }

}
