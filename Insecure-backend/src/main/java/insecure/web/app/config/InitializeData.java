package insecure.web.app.config;

import insecure.web.app.entity.User;
import insecure.web.app.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitializeData implements CommandLineRunner {

    private final UserRepository userRepository;

    public InitializeData(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        User user1 = User.builder()
                .username("ivan")
                .password("sifra2")
                .role("admin")
                .build();

        User user2 = User.builder()
                .username("mirko")
                .password("sifra")
                .role("student")
                .build();

        this.userRepository.save(user1);
        this.userRepository.save(user2);
    }
}
