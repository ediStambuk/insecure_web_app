package insecure.web.app.controller;

import insecure.web.app.entity.Comment;
import insecure.web.app.repository.CommentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    private final CommentRepository commentRepository;

    public CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @GetMapping()
    public ResponseEntity<List<Comment>> findAllComments() {
        return new ResponseEntity<>(this.commentRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Comment> findCommentById(@PathVariable("id") Long id) {
        if (id == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(this.commentRepository.findById(id).orElse(null), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<List<Comment>> createComment(@RequestBody Comment comment) {
        if (comment == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        this.commentRepository.save(comment);
        return new ResponseEntity<>(this.commentRepository.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCommentById(@PathVariable("id") Long id) {
        if (id == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (this.commentRepository.findById(id).isPresent()) {
            this.commentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
