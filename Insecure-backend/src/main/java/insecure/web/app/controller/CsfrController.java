package insecure.web.app.controller;

import insecure.web.app.entity.Product;
import insecure.web.app.entity.User;
import insecure.web.app.helpers.JwtHelper;
import insecure.web.app.repository.ProductRepository;
import insecure.web.app.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/csfr")
public class CsfrController {

    private final JwtHelper jwtHelper;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    public CsfrController(JwtHelper jwtHelper, ProductRepository productRepository, UserRepository userRepository) {
        this.jwtHelper = jwtHelper;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/login/{username}/{password}")
    public ResponseEntity<String> login(@PathVariable("username") String username,
                                        @PathVariable("username") String password) {
        User user = this.userRepository.findUserByUsernameAndPassword(username, password);
        if (user != null) {
            String token = this.jwtHelper.generateToken(username);
            return new ResponseEntity<>(token, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/buyProduct/{username}/{token}")
    public ResponseEntity<Product> buyProduct(@PathVariable("username") String username,
                                              @PathVariable("token") String token,
                                              @RequestBody Product product) {
        if (this.jwtHelper.validateToken(token, username)) {
            product.setUsername(username);
            this.productRepository.save(product);
            return new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }
}
