package insecure.web.app.controller;

import insecure.web.app.dto.ChangePassword;
import insecure.web.app.entity.User;
import insecure.web.app.repository.UserRepository;
import insecure.web.app.repository.UserRepositorySQL;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserRepositorySQL userRepositorySQL;
    private final UserRepository userRepository;

    public UserController(UserRepositorySQL userRepositorySQL,
                          UserRepository userRepository) {
        this.userRepositorySQL = userRepositorySQL;
        this.userRepository = userRepository;
    }

    @PostMapping("/login")
    public ResponseEntity<List<String>> login(@RequestBody User user) {
        List<String> listOfUsers = this.userRepositorySQL.login(user.getUsername(),
                user.getPassword());

        if (listOfUsers == null || listOfUsers != null && listOfUsers.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(listOfUsers, HttpStatus.OK);
    }

    @PostMapping("/changePassword")
    public ResponseEntity<Void> changePassword(@RequestBody ChangePassword user) {
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User fetchedUser = this.userRepository.findUserByUsernameAndPassword(user.getUsername(), user.getOldPassword());
        if (fetchedUser != null) {
            fetchedUser.setPassword(user.getNewPassword());

            this.userRepository.save(fetchedUser);

            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
