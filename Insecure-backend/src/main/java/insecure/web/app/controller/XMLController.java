package insecure.web.app.controller;

import insecure.web.app.entity.XMLEntity;
import insecure.web.app.parser.XMLparser;
import insecure.web.app.repository.XMLEntityRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@RestController
@RequestMapping("/api/xml")
public class XMLController {

    private final XMLEntityRepository xmlEntityRepository;

    public XMLController(XMLEntityRepository xmlEntityRepository) {
        this.xmlEntityRepository = xmlEntityRepository;
    }

    @PostMapping()
    public String saveXml(@RequestPart("file") MultipartFile file) {
        if (file == null) {
            System.err.println("file wasn't send");
        }
        String text = "";
        try {
            File xmlFile = File.createTempFile("tempFile", "xml");
            byte[] bytes = file.getBytes();
            OutputStream os = new FileOutputStream(xmlFile);
            os.write(bytes);
            os.close();

            text = XMLparser.parseXMLFile(xmlFile);
            XMLEntity xmlEntity = XMLEntity.builder().configuration(text).build();
            this.xmlEntityRepository.save(xmlEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
}
