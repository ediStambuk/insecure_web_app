package insecure.web.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class XMLEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String configuration;
}
