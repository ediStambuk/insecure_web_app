package insecure.web.app.parser;

import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

@Slf4j
public class XMLparser {

    public static String parseXMLFile(File file) {
        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document doc = db.parse(file);
            NodeList nodeList = doc.getElementsByTagName("configuration");

            Node node = nodeList.item(0);

            String text = node.getTextContent();

            System.out.println(text);
            log.info("xml file " + text);
            return text;
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            System.out.println("Exception Parsing XML");
        }
        return "";
    }
}
