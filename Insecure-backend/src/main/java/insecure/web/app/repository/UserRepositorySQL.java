package insecure.web.app.repository;

import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Component
public class UserRepositorySQL {

    private final DataSource dataSource;

    protected UserRepositorySQL(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<String> login(String username, String password) {
        try {
            String sql = "select "
                    + "* "
                    + "from USER where username = '"
                    + username
                    + "' and password = '"
                    + password
                    + "'";

            Connection c = dataSource.getConnection();
            ResultSet rs = c.createStatement().executeQuery(sql);
            List<String> result = new LinkedList<>();
            if (rs.next()) {
                for (int i = 0; i < 4; i++) {
                    try {
                        result.add(rs.getString(i));
                    } catch (Exception e) {
                        System.err.println("Can't access column " + i);
                    }
                }
            }

            return result;
        } catch (SQLException e) {
            System.err.println(e);
        }

        return null;
    }
}
