package insecure.web.app.repository;

import insecure.web.app.entity.XMLEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface XMLEntityRepository extends JpaRepository<XMLEntity, Long> {
}
