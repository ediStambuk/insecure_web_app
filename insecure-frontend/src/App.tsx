import React from 'react';
import './App.css';
import {Nav, NavItem, NavLink} from "reactstrap";
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import SQLInjection from "./pages/SqlInjection";
import XssDom from "./pages/XssDom";
import XssStored from "./pages/XssStored";
import Dashboard from "./pages/Dashboard";
import XmlUpload from "./pages/XmlUpload";
import Csfr from "./pages/Csfr";
import ReDos from "./pages/Reggdos";

function App() {
  return (
    <div className="App">
      <Nav vertical tabs={true} className="left">
        <h1 className="heading">Insecure app</h1>
        <NavItem className="navitem">
          <NavLink href="/sqlInjection">
            SQL injection
          </NavLink>
        </NavItem>
        <NavItem className="navitem">
          <NavLink href="/xssDom?label=SomeLabelFromUrl">
            DOM XSS
          </NavLink>
        </NavItem>
        <NavItem className="navitem">
          <NavLink href="/xssStored">
            Stored XSS
          </NavLink>
        </NavItem>
        <NavItem className="navitem">
          <NavLink href="/xmlUpload">
            XML upload
          </NavLink>
        </NavItem>
        <NavItem className="navitem">
          <NavLink href="/redos">
            ReDos
          </NavLink>
        </NavItem>
      </Nav>

      <div className="right">
        <Router>
          <Routes>
            <Route path="/" element={<Dashboard/>}/>
            <Route path="/sqlInjection" element={<SQLInjection/>}/>
            <Route path="/xssDom" element={<XssDom/>}/>
            <Route path="/xssStored" element={<XssStored/>}/>
            <Route path="/xmlUpload" element={<XmlUpload/>}/>
            <Route path="/csfr" element={<Csfr/>}/>
            <Route path="/redos" element={<ReDos/>}/>
          </Routes>
        </Router>
      </div>

    </div>
  );
}

export default App;
