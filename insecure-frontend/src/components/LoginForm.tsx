import {Alert, Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import {useState} from "react";
import axios from "axios";

interface LoginProp {
  loginToBackend: (username: string, password: string) => Promise<any>;
}

function LoginFrom(props: LoginProp) {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);

  const login = () => {
    //axios.post("http://localhost:8080/api/user/login", {username, password})
    props.loginToBackend(username, password)
        .then((res) => {
          console.log("Login " + JSON.stringify(res));
          if (res.status === 200) {
            setShowErrorMessage(false);
            setShowSuccessMessage(true);
          } else {
            setShowErrorMessage(true);
            setShowSuccessMessage(false);
          }
        }).catch((error) => {
      setShowErrorMessage(true);
      setShowSuccessMessage(false);
    })
  }

  return (
      <>
        <h2>Login screen</h2>

        {showErrorMessage &&
        <Alert
            color="warning"
            toggle={() => setShowErrorMessage(false)}
        >
            You have not logged in
        </Alert>}

        {showSuccessMessage &&
        <Alert
            color="info"
            toggle={() => setShowSuccessMessage(false)}
        >
            You have logged in successfully
        </Alert>}

        <Container>
          <Form inline>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Username
              </Label>
              <Input
                  id="username"
                  name="username"
                  placeholder="username"
                  type="text"
                  value={username}
                  onChange={event => setUsername(event.target.value)}
              />
            </FormGroup>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Password
              </Label>
              <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="text"
                  value={password}
                  onChange={event => setPassword(event.target.value)}
              />
            </FormGroup>
            <Button onClick={login}>
              Submit
            </Button>
          </Form>
        </Container>
      </>
  );
}

export default LoginFrom;
