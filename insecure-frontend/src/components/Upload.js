import React, {useState} from "react";
import Files from "react-butterfiles";

const Upload = ({accept, saveFile}) => {

    const [file, setFile] = useState(undefined);
    const [error, setError] = useState([]);

    const saveFileRemote = (file) => {
        setFile(file);
        saveFile(file);
    }

    return (
        <div>
            <h1>Upload Files</h1>

            <Files
                multiple={false} maxSize="2mb" multipleMaxSize="10mb" accept={accept}
                onSuccess={files => saveFileRemote(files)}
                onError={errors => setError(errors)}
            >
                {({ browseFiles }) => (
                    <>
                        <button onClick={browseFiles}>Upload</button>
                    </>
                )}
            </Files>
        </div>
    );
};

export default Upload;
