import {useState} from "react";
import {Alert, Button, Container, Form, FormGroup, Input, Label} from "reactstrap";
import axios from "axios";
const isEmail = require('validator/lib/isEmail');

function ReDos() {

  const [username, setUsername] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [password, setPassword] = useState("");
  const [repPassword, setRepPassword] = useState("");
  const [email, setEmail] = useState("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [showSuccessMessage, setShowSuccessMessage] = useState(false);

  const changePassword = () => {
    if (!isEmail(email)) {
      alert('email is not correct');
    }


    axios.post("http://localhost:8080/api/user/changePassword", {username, oldPassword, newPassword: password})
        .then((res) => {
          console.log("Login " + JSON.stringify(res));
          if (res.status === 200) {
            setShowErrorMessage(false);
            setShowSuccessMessage(true);
          } else {
            setShowErrorMessage(true);
            setShowSuccessMessage(false);
          }
        }).catch((error) => {
      setShowErrorMessage(true);
      setShowSuccessMessage(false);
    })
  }

  return (
      <>
        <h2>Change password</h2>

        {showErrorMessage &&
            <Alert
                color="warning"
                toggle={() => setShowErrorMessage(false)}
            >
                You have not logged in
            </Alert>}

        {showSuccessMessage &&
            <Alert
                color="info"
                toggle={() => setShowSuccessMessage(false)}
            >
                You have logged in successfully
            </Alert>}

        <Container>
          <Form inline>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Username
              </Label>
              <Input
                  id="username"
                  name="username"
                  placeholder="username"
                  type="text"
                  value={username}
                  onChange={event => setUsername(event.target.value)}
              />
            </FormGroup>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Old password
              </Label>
              <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  value={oldPassword}
                  onChange={event => setOldPassword(event.target.value)}
              />
            </FormGroup>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Password
              </Label>
              <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  value={password}
                  onChange={event => setPassword(event.target.value)}
              />
            </FormGroup>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Repeated Password
              </Label>
              <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  value={repPassword}
                  onChange={event => setRepPassword(event.target.value)}
              />
            </FormGroup>
            <FormGroup className="mb-2 me-sm-2 mb-sm-0">
              <Label
                  className="me-sm-2"
              >
                Email
              </Label>
              <Input
                  id="examplePassword"
                  name="password"
                  placeholder="email"
                  type="email"
                  value={email}
                  onChange={event => setEmail(event.target.value)}
              />
            </FormGroup>
            <Button onClick={changePassword}>
              Submit
            </Button>
          </Form>
        </Container>
      </>
  );
}

export default ReDos;
