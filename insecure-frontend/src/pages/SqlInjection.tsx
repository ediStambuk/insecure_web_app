import axios from "axios";
import LoginFrom from "../components/LoginForm";

function SQLInjection() {

  return (
      <>
        <h1>Sql injection</h1>
        <br/>
        <LoginFrom loginToBackend={(username, password) =>
            axios.post("http://localhost:8080/api/user/login", {username, password})}/>
      </>
  );
}

export default SQLInjection;
