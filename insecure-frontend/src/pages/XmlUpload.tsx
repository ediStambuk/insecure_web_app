import React from 'react';
import Upload from "../components/Upload";
import axios from "axios";

const XmlUpload = () => {

  const callXmlService = (file: any) => {
    const formData = new FormData();
    formData.append("title", file[0]?.name);
    formData.append("file", file[0]?.src?.file);

    axios({
      url: 'http://localhost:8080/api/xml',
      method: 'POST',
      data: formData,
      headers: {
        'Content-Type': "multipart/form-data"
      }
    }).then((response) => {
      console.log(response);
    }).catch((error) => {
      console.log("file upload error " + error);
    })
  }

  return (
      <Upload accept={['text/xml']} saveFile={callXmlService}/>
  );
};

export default XmlUpload;
