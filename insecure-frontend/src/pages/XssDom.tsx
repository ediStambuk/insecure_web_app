import {useSearchParams} from "react-router-dom";
import DOMPurify from 'dompurify';

function XssDom() {

  const [searchParams] = useSearchParams();

  const xssLabel = searchParams.get("label") || "";
  const sanitizedLabel = DOMPurify.sanitize(xssLabel);

  return (
      <h1><div dangerouslySetInnerHTML={{__html: sanitizedLabel}}/></h1>
  );
}

export default XssDom;
