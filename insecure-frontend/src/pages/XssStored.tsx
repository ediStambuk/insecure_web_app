import {useEffect, useState} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {Container, ListGroup} from "react-bootstrap";
import axios from "axios";
import {Comment} from '../models/Comment';

function XssStored() {

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const [comments, setComments] = useState([] as Comment[]);

  useEffect(() => {
    axios.get('http://localhost:8080/api/comment').then((res) => {
      if (res.status === 200) {
        setComments(res.data as Comment[]);
      }
    }).catch((error) => {});
  }, [])

  const saveComment = () => {
    axios.post("http://localhost:8080/api/comment", {title, description})
        .then((res) => {
          console.log("Comment save " + JSON.stringify(res));
          if (res.status === 200) {
            setComments(res.data as Comment[]);
          }
        }).catch((error) => {})
  }

  const showComment = (comment: Comment) => {
    return <ListGroup.Item
        as="li"
        className="d-flex justify-content-between align-items-start"
    >
      <div className="ms-2 me-auto">
        <div className="fw-bold">{comment.title}</div>
        <div dangerouslySetInnerHTML={{__html: comment.description}}/>
      </div>
    </ListGroup.Item>
  }

  return (
      <>
        <h1>XSS</h1>
    <Container>
      <Form inline>
        <FormGroup className="mb-2 me-sm-2 mb-sm-0">
          <Label
              className="me-sm-2"
          >
            Title
          </Label>
          <Input
              id="username"
              name="username"
              placeholder="Title"
              type="text"
              value={title}
              onChange={event => setTitle(event.target.value)}
          />
        </FormGroup>
        <FormGroup className="mb-2 me-sm-2 mb-sm-0">
          <Label
              className="me-sm-2"
          >
            Description
          </Label>
          <Input
              id="examplePassword"
              name="password"
              placeholder="description"
              type="text"
              value={description}
              onChange={event => setDescription(event.target.value)}
          />
        </FormGroup>
        <Button onClick={saveComment}>
          Submit
        </Button>
      </Form>
    </Container>
    <ListGroup as="ol">
      {comments.map((comment) => showComment(comment))}
    </ListGroup>
  </>
  );
}

export default XssStored;
